from App.bs4.beautiful_book import BeautifulBook


class BeautifulBookInfo:
    soup = ''
    book_title = ''
    book_desc = ''
    book_cover_url = ''
    book_chapter_list = []

    def __init__(self, document):
        self.soup = BeautifulBook(document)

    def getBookTitle(self, name, className, DOMId):
        self.book_title = self.soup.find_all_text(name, className, DOMId)
        return self.book_title

    def getBookDesc(self, name, className, DOMId):
        self.book_desc = self.soup.find_all_text(name, className, DOMId)
        return self.book_desc

    def getBookCoverImageUrl(self, name, className, DOMId):
        self.book_cover_url = self.soup.find_all_image_url(name, className, DOMId)
        return self.book_cover_url

    def getBookChapterLink(self, name, className, DOMId):
        self.book_chapter_list = self.soup.find_all_link(name, className, DOMId)
        return self.book_chapter_list
