from App.bs4.beautiful_book import BeautifulBook


class BeautifulContentInfo:
    soup = ''
    chapter_title = ''
    content = ''

    def __init__(self, document):
        self.soup = BeautifulBook(document)

    def getChapterTitle(self, name, className, DOMId):
        self.chapter_title = self.soup.find_all_text(name, className, DOMId)
        return self.chapter_title

    def getContent(self, name, className, DOMId):
        self.content = self.soup.find_all_text(name, className, DOMId)
        return self.content
