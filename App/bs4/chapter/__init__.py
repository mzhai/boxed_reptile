from App.bs4.beautiful_book import BeautifulBook


class BeautifulChapterInfo:
    soup = ''
    chapter_title = ''

    def __init__(self, document):
        self.soup = BeautifulBook(document)

    def getChapterTitle(self, name, className, DOMId):
        self.chapter_title = self.soup.find_all_text(name, className, DOMId)
        return self.chapter_title
