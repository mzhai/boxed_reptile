from bs4 import BeautifulSoup


class BeautifulBook:
    soup = BeautifulSoup()
    document = ''

    def __init__(self, document):
        self.document = document
        self.soup = BeautifulSoup(document)

    def find_all(self, name=None, className=None, DOMId=None):
        return self.soup.find_all(name, class_=None if className is None else '{}'.format(className),
                                  id=None if DOMId is None else '{}'.format(DOMId))

    def find_all_text(self, name=None, className=None, DOMId=None):
        beautifulBook = self.find_all(name, className, DOMId)
        string_document = ''
        if beautifulBook is not None:
            for content in beautifulBook:
                string_document = '{0}{1}'.format(string_document, content)
        str_soup = BeautifulSoup(string_document)
        string_result = ''
        for strings in str_soup.strings:
            string_result = '{0}{1}'.format(string_result, strings)
        return string_result

    def find_all_image_url(self, name=None, className=None, DOMId=None):
        beautifulBook = self.find_all(name, className, DOMId)

        string_document = ''
        if beautifulBook is not None:
            for content in beautifulBook:
                string_document = '{0}{1}'.format(string_document, content)
        str_soup = BeautifulSoup(string_document)

        img_url_list = str_soup.find_all('img')

        cover_url_list = []
        for img in img_url_list:
            if img['src'] is not None:
                cover_url_list.append(img['src'])
        if len(cover_url_list) > 0:
            return cover_url_list[0]
        else:
            return ''

    def find_all_link(self, name=None, className=None, DOMId=None):
        beautifulBook = self.find_all(name, className, DOMId)

        string_document = ''
        if beautifulBook is not None:
            for content in beautifulBook:
                string_document = '{0}{1}'.format(string_document, content)
        str_soup = BeautifulSoup(string_document)
        url_list = str_soup.find_all('a')

        a_link_href_list = []
        for a in url_list:
            if a['href'] is not None:
                ainfo = {
                    'href': a['href'],
                    'text': a.string
                }
                a_link_href_list.append(ainfo)
        return a_link_href_list
