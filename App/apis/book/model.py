from App.common.baseDict import BaseDict
from exts import db


class Book(BaseDict, db.Model):
    superKeys = (
        'id',
        'repo_id',
        'repo_url',
        'repo_name',
        'book_name',
        'book_url',
        'book_cover_image_file',
        'book_cover_image_url'
    )
    id = db.Column(db.Integer, primary_key=True)
    repo_id = db.Column(db.Integer)
    repo_url = db.Column(db.String(80))
    repo_name = db.Column(db.String(80))
    book_name = db.Column(db.String(255), unique=True)
    book_url = db.Column(db.String(255))
    book_cover_image_file = db.Column(db.LargeBinary)
    book_cover_image_url = db.Column(db.String)

    def __init__(self):
        BaseDict.__init__(self, superKeys=self.superKeys)
