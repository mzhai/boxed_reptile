import json

import requests
from flask import Blueprint, request

from App.apis.book.model import Book
from App.apis.repo import query_repo_by_id
from App.bs4 import beautiful_request
from App.bs4.book import BeautifulBookInfo
from App.common.baseRequest import FormValidate
from App.common.baseResponse import ErrorResponse, SuccessResponse
from exts import db
import time
from App.email import send_text_email
from App.common.baseSession import BaseSession

book = Blueprint('book', __name__)


# 通过书名查找书籍信息
def query_book_by_bookname(name):
    book_by_name = Book.query.filter_by(book_name='{}'.format(name)).first()
    return book_by_name


# 通过书id查找书籍信息
def query_book_by_bookid(id):
    book_by_id = Book.query.filter_by(id='{}'.format(id)).first()
    return book_by_id


# 查找书籍列表
def query_book_list():
    repoList = Book.query.all()
    content = []
    for x in repoList:
        content.append(x.toDict())
    response = {
        'count': len(repoList),
        'content': content
    }
    return response


# 测试
@book.route('/test')
def test():
    return 'test book ok!'


# API添加书籍
@book.route('/api/book/add', methods=['POST'])
@BaseSession.check
def book_add():
    book_info_request = {
        'repo_id': request.form.get('repo_id'),
        'repo_name': request.form.get('repo_name'),
        'repo_url': request.form.get('repo_url'),

        'book_name': request.form.get('book_name'),
        'book_url': request.form.get('book_url'),
        'book_cover_image_file': request.form.get('book_cover_image_file'),
        'book_cover_image_url': request.form.get('book_cover_image_url'),
    }
    fm = FormValidate()
    fm.required_spec(book_info_request, ['repo_id', 'repo_name', 'repo_url', 'book_name', 'book_url'])
    fm.valid()

    if fm.toDict().get('error') is False:
        book_by_name = query_book_by_bookname(book_info_request.get('book_name'))
        repo_by_id = query_repo_by_id(book_info_request.get('repo_id'))
        if book_by_name is None:
            if repo_by_id is not None:
                bookin = Book()
                bookin.repo_id = book_info_request.get('repo_id')
                bookin.repo_name = book_info_request.get('repo_name')
                bookin.repo_url = book_info_request.get('repo_url')
                bookin.book_name = book_info_request.get('book_name')
                bookin.book_url = book_info_request.get('book_url')
                bookin.book_cover_image_file = book_info_request.get('book_cover_image_file')
                bookin.book_cover_image_url = book_info_request.get('book_cover_image_url')
                db.session.add(bookin)
                db.session.commit()
                return SuccessResponse().toJson()
            else:
                return ErrorResponse(message='源不存在').toJson(), 412
        else:
            return ErrorResponse(message='书籍已存在').toJson(), 412
    else:
        return ErrorResponse(message='{}'.format(fm.toDict().get('error_message'))).toJson(), 412


# API 获取书籍列表
@book.route('/api/book/list')
def book_list():
    return SuccessResponse(result=query_book_list()).toJson()


# http://www.xbiquge.la/10/10489/
def get_book_info(book_url):
    document = beautiful_request(book_url)
    beautifulBook = BeautifulBookInfo(document)
    beautifulBook.getBookTitle('div', None, 'info')
    beautifulBook.getBookDesc('div', None, 'intro')
    beautifulBook.getBookCoverImageUrl('div', None, 'fmimg')
    beautifulBook.getBookChapterLink('dl', None, None)
    beautifulBook_title = beautifulBook.book_title
    beautifulBook_desc = beautifulBook.book_desc
    beautifulBook_cover_url = beautifulBook.book_cover_url
    beautifulBook_chapter_list = beautifulBook.book_chapter_list
    return {
        'bookName': beautifulBook_title,
        'bookDesc': beautifulBook_desc,
        'bookCoverUrl': beautifulBook_cover_url,
        'bookChapterList': beautifulBook_chapter_list
    }


@book.route('/api/book/getBookInfo', methods=['POST'])
def get_book_info_api():
    book_info_request = {
        'repo_id': request.form.get('repo_id'),
        'repo_name': request.form.get('repo_name'),
        'repo_url': request.form.get('repo_url'),
        'book_url': request.form.get('book_url'),
    }
    fm = FormValidate()
    fm.required_spec(book_info_request, ['repo_id', 'book_url'])
    fm.valid()

    if fm.toDict().get('error') is False:

        result = get_book_info(book_info_request.get('book_url'))

        book_by_name = query_book_by_bookname(result.get('bookName'))
        repo_by_id = query_repo_by_id(book_info_request.get('repo_id'))

        if book_by_name is None:
            if repo_by_id is not None:
                bookin = Book()
                bookin.repo_id = request.form.get('repo_id')
                bookin.repo_name = repo_by_id.toDict().get('repo_name')
                bookin.repo_url = repo_by_id.toDict().get('repo_url')
                bookin.book_name = result.get('bookName')
                bookin.book_url = book_info_request.get('book_url')
                bookin.book_cover_image_file = result.get('book_cover_image_file')
                bookin.book_cover_image_url = result.get('bookCoverUrl')
                db.session.add(bookin)
                db.session.commit()
                return SuccessResponse(result=bookin.toDict()).toJson(), 200
            else:
                return ErrorResponse(message='源不存在').toJson(), 412
        else:
            return ErrorResponse(message='书籍已存在',result=book_by_name.toDict()).toJson(), 412
    else:
        return ErrorResponse(message='{}'.format(fm.toDict().get('error_message'))).toJson(), 412



# 爬取全本
@book.route('/api/book/getAllBook', methods=['POST'])
@BaseSession.check
def get_all_book():
    book_info_request = {
        'book_url': request.form.get('book_url'),
        'repo_id': request.form.get('rep'
                                    'o_id'),
    }
    fm = FormValidate()
    fm.required_spec(book_info_request, ['book_url'])
    fm.valid()

    if fm.toDict().get('error') is False:

        response_dict = {
            'book_request_response': '',
            'chapter_request_response': '',
            'content_request_response': ''
            # 'signin_response':''
        }

        # 认证
        # Todo: 可能需要设置一个管理员账号
        # signin_request_data = {
        #     'accountNumber': (None, 'test'),
        #     'password': (None, 'test')
        # }
        # signin_response = requests.request("POST", 'http://localhost:5000/api/common/users/signin/',
        #                                          files=signin_request_data)
        # response_dict['signin_response'] = json.loads(signin_response.text)

        # Todo 需要形成调用链，自动化存储
        # 在json模块有2个方法，
        #
        # loads()：将json数据转化成dict数据
        # dumps()：将dict数据转化成json数据
        # load()：读取json文件数据，转成dict数据
        # dump()：将dict数据转化成json数据后写入json文件
        # 爬取书籍信息
        get_book_request_data = {
            'repo_id': (None, book_info_request.get('repo_id')),
            'book_url': (None, book_info_request.get('book_url'))
        }
        book_request_response = requests.request("POST", 'http://localhost:5000/api/book/getBookInfo',
                                                 files=get_book_request_data)
        response_dict['book_request_response'] = json.loads(book_request_response.text)

        book_id = response_dict['book_request_response'].get('result').get('id')
        book_url = response_dict['book_request_response'].get('result').get('book_url')
        repo_url = response_dict['book_request_response'].get('result').get('repo_url')
        # 爬取章节信息
        get_chapter_request_data = {
            'book_id': (None, book_id),
            'book_url': (None, book_url),
            'repo_url': (None, repo_url)
        }
        chapter_request_response = requests.request("POST", 'http://localhost:5000/api/chapter/getChapterList',
                                                    files=get_chapter_request_data)
        response_dict['chapter_request_response'] = json.loads(chapter_request_response.text)

        chapter_list = response_dict['chapter_request_response'].get('result')
        # 循环爬取内容
        content_request_result = []
        for chapter in chapter_list:
            get_content_request_data = {
                'chapter_id': (None, chapter.get('id')),
                'chapter_url': (None, chapter.get('chapter_url')),
            }
            content_request_response = requests.request("POST", 'http://localhost:5000/api/content/getContentInfo',
                                                        files=get_content_request_data)
            content_request_result.append(json.loads(content_request_response.text))
            # time.sleep(7)
            print(chapter.get('id'))
            print(chapter.get('chapter_url'))
        response_dict['content_request_response'] = content_request_result


        return SuccessResponse(result=response_dict).toJson(), 200
    else:
        return ErrorResponse(message='{}'.format(fm.toDict().get('error_message'))).toJson(), 412


@book.route('/api/book/sendEmail',methods=['POST'])
@BaseSession.check
def send_email():
    email_request = {
        'email_text': request.form.get('email_text'),
    }
    fm = FormValidate()
    fm.required_spec(email_request, ['email_text'])
    fm.valid()

    if fm.toDict().get('error') is False:
        res = send_text_email(email_request.get('email_text'))
        if res.get('success') is True:
            return SuccessResponse().toJson(), 200
        else:
            return ErrorResponse(message='邮件发送失败，请检查！').toJson(), 412
    else:
        return ErrorResponse(message='{}'.format(fm.toDict().get('error_message'))).toJson(), 412

