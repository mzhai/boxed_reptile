from flask import Blueprint

from App.apis.book import book
from App.apis.book.model import Book
from App.apis.chapter import chapter
from App.apis.chapter.model import Chapter
from App.apis.common.menu import menu
from App.apis.common.menu.model import Menu
from App.apis.common.users import user
from App.apis.common.users.model import User
from App.apis.content import content
from App.apis.content.model import Content
# apis
from App.apis.repo import repo
# model
from App.apis.repo.model import Repo
# utils
from App.common.baseResponse import SuccessResponse, ErrorResponse
# db
from exts import db

init = Blueprint('init', __name__)


def init_apis(app):
    app.register_blueprint(init)
    app.register_blueprint(repo)
    app.register_blueprint(book)
    app.register_blueprint(chapter)
    app.register_blueprint(content)
    app.register_blueprint(menu)
    app.register_blueprint(user)


@init.route('/init/db/')
def init_db():
    try:
        db.create_all()
        return SuccessResponse(message='创建成功').toJson()
    except Exception as e:
        return ErrorResponse(message='{}'.format(e)).toJson()
