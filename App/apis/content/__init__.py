from flask import Blueprint, request

from App.apis.content.model import Content
from App.bs4 import beautiful_request
from App.bs4.content import BeautifulContentInfo
from App.common.baseRequest import FormValidate
from App.common.baseResponse import ErrorResponse, SuccessResponse
from exts import db

content = Blueprint('content', __name__)


# 通过书id查找书籍信息
def query_chapter_by_chapterid(id):
    chapter_by_id = Content.query.filter_by(chapter_id='{}'.format(id)).first()
    return chapter_by_id


def get_chapter_content(chapter_url):
    document = beautiful_request(chapter_url)
    beautifulBook = BeautifulContentInfo(document)
    beautifulBook.getChapterTitle('div', 'bookname', None)
    beautifulBook.getContent('div', None, 'content')
    beautifulContent_ChapterTitle = beautifulBook.chapter_title
    beautifulContent = beautifulBook.content
    return {
        'chapterTitle': beautifulContent_ChapterTitle,
        'content': beautifulContent
    }


def intoContent(chapter_title, content, chapter_id):
    contentin = Content()
    contentin.chapter_title = chapter_title
    contentin.content = content
    contentin.chapter_id = chapter_id
    db.session.add(contentin)
    db.session.commit()


@content.route('/api/content/getContentInfo', methods=['POST'])
def get_content_info():
    book_info_request = {
        'chapter_id': request.form.get('chapter_id'),
        'chapter_url': request.form.get('chapter_url'),
    }
    fm = FormValidate()
    fm.required_spec(book_info_request, ['chapter_id', 'chapter_url'])
    fm.valid()

    if fm.toDict().get('error') is False:

        chapter_by_chapterid = query_chapter_by_chapterid(book_info_request.get('chapter_id'))
        if chapter_by_chapterid is None :
            result = get_chapter_content(book_info_request.get('chapter_url'))
            if result is not None:
                print('已获取内容')
                intoContent(result.get('chapterTitle'), result.get('content'), book_info_request.get('chapter_id'))
                return SuccessResponse().toJson()
            else:
                return ErrorResponse(message='无内容').toJson(), 412

        else:
            print('已存在')
            return ErrorResponse(message='章节已存在').toJson(), 412
    else:
        return ErrorResponse(message='{}'.format(fm.toDict().get('error_message'))).toJson(), 412
