from exts import db
from App.common.baseDict import BaseDict

class Content(BaseDict, db.Model):
    superKeys = (
        'id',
        'chapter_title',
        'chapter_id',
        'content'
    )
    id = db.Column(db.Integer, primary_key=True)
    chapter_title = db.Column(db.String(80), unique=True)
    chapter_id = db.Column(db.Integer)
    content = db.Column(db.Text)

    def __init__(self):
        BaseDict.__init__(self, superKeys=self.superKeys)