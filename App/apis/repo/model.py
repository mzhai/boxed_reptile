from App.common.baseDict import BaseDict
from exts import db


class Repo(BaseDict, db.Model):
    superKeys = 'id', 'repo_name', 'repo_url', 'search_keys'
    id = db.Column(db.Integer, primary_key=True)
    repo_name = db.Column(db.String(80), unique=True)
    repo_url = db.Column(db.String(80))
    search_keys = db.Column(db.String(255))

    def __init__(self):
        BaseDict.__init__(self, superKeys=self.superKeys)
