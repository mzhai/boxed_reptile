from flask import Blueprint, request

from App.apis.repo.model import Repo
from App.common.baseRequest import FormValidate
from App.common.baseResponse import SuccessResponse, ErrorResponse
from exts import db

repo = Blueprint('repo', __name__)


def query_repo_by_name(repo_name):
    repo_by_name = Repo.query.filter_by(repo_name='{}'.format(repo_name)).first()
    return repo_by_name


def query_repo_by_id(repo_id):
    repo_by_id = Repo.query.filter_by(id='{}'.format(repo_id)).first()
    return repo_by_id


def query_repo_list():
    repoList = Repo.query.all()
    content = []
    for x in repoList:
        content.append(x.toDict())
    response = {
        'count': len(repoList),
        'content': content
    }
    return response


@repo.route('/api/repo/searchRepoByName/', methods=['POST'])
def searchRepoByName():
    repo_request = {
        'repo_name': request.form.get('repo_name')
    }
    fm = FormValidate()
    fm.required(repo_request)
    fm.valid()
    search_repo_by_name = query_repo_by_name(repo_request.get('repo_name'))
    if (fm.toDict().get('error') is False) and (search_repo_by_name is not None):
        return SuccessResponse(result=search_repo_by_name.toDict()).toJson()
    else:
        return ErrorResponse(message='{}'.format(fm.toDict().get('error_message'))).toJson(), 412


@repo.route('/api/repo/list/', methods=['POST'])
def repo_list():
    return SuccessResponse(result=query_repo_list()).toJson()


@repo.route('/api/repo/add/', methods=['POST'])
def add_repo():
    if request.method == 'POST':
        repo_request = {
            'repo_name': request.form.get('repo_name', default=None),
            'repo_url': request.form.get('repo_url', default=None),
            'search_keys': request.form.get('search_keys', default=None),
        }
        query_name = query_repo_by_name(repo_request.get('repo_name'))
        fm = FormValidate()
        fm.required(repo_request)
        fm.valid()
        if not fm.toDict().get('error'):
            if query_name is not None:
                return ErrorResponse(message='源名称重复').toJson(), 412
            else:
                repo_in = Repo()
                repo_in.repo_name = repo_request.get('repo_name')
                repo_in.repo_url = repo_request.get('repo_url')
                repo_in.search_keys = repo_request.get('search_keys')
                db.session.add(repo_in)
                db.session.commit()
                return SuccessResponse().toJson()
        else:
            return ErrorResponse(message='{}'.format(fm.toDict().get('error_message'))).toJson(), 412


@repo.route('/api/repo/delete/', methods=['POST'])
def delete_repo_by_id():
    error = None
    repo_request = {
        'id': request.form.get('id')
    }
    fm = FormValidate()
    fm.required(repo_request)
    fm.valid()
    if not fm.toDict().get('error'):
        repo_by_id = Repo.query.filter_by(id='{}'.format(id)).first()
        if repo_by_id is not None:
            db.session.delete(repo_by_id)
            db.session.commit()
            return SuccessResponse().toJson(), error
        else:
            error = 412
            return ErrorResponse(message='要删除的对象不存在').toJson(), error
    else:
        return ErrorResponse(message='{}'.format(fm.toDict().get('error_message'))).toJson(), 412
