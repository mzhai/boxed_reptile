import re

from flask import Blueprint, request

from App.apis.chapter.model import Chapter
from App.bs4 import beautiful_request
from App.bs4.book import BeautifulBookInfo
from App.bs4.chapter import BeautifulChapterInfo
from App.common.baseRequest import FormValidate
from App.common.baseResponse import ErrorResponse, SuccessResponse
from exts import db

chapter = Blueprint('chapter', __name__)


# 通过书id查找书籍章节信息
def query_book_by_bookid(id):
    book_by_id = Chapter.query.filter_by(book_id='{}'.format(id)).first()
    return book_by_id


def query_chapters_by_bookid(bookid):
    chapters_by_bookid = Chapter.query.filter_by(book_id='{}'.format(bookid)).all()
    return chapters_by_bookid

def get_chapter_list(book_url):
    document = beautiful_request(book_url)
    beautifulBook = BeautifulBookInfo(document)
    beautifulBook.getBookChapterLink('dl', None, None)
    beautifulBook_chapter_list = beautifulBook.book_chapter_list
    return beautifulBook_chapter_list


def intoChapter(chapter_title, book_id, chapter_url):
    chapterin = Chapter()
    chapterin.chapter_title = chapter_title
    chapterin.book_id = book_id
    chapterin.chapter_url = chapter_url
    db.session.add(chapterin)
    db.session.commit()


@chapter.route('/api/chapter/getChapterList', methods=['POST'])
def get_chapter_list_api():
    book_info_request = {
        'book_id': request.form.get('book_id'),
        'book_name': request.form.get('book_name'),
        'book_url': request.form.get('book_url'),
        'repo_url': request.form.get('repo_url'),
    }
    fm = FormValidate()
    fm.required_spec(book_info_request, ['book_id', 'book_url', 'repo_url'])
    fm.valid()
    if fm.toDict().get('error') is False:
        result = get_chapter_list(book_info_request.get('book_url'))
        # (book_info_request.get('book_url'))
        book_by_bookid = query_book_by_bookid(book_info_request.get('book_id'))
        if book_by_bookid is None and result is not None and len(result) > 0:

            for urlItem in result:

                if re.match(r'^https?:/{2}\w.+$', urlItem.get('href')):
                    urlResult = urlItem.get('href')
                else:
                    urlResult = '{0}{1}'.format(book_info_request.get('repo_url'), urlItem.get('href'))

                intoChapter(urlItem.get('text'), book_info_request.get('book_id'), urlResult)

            chapters = query_chapters_by_bookid(book_info_request.get('book_id'))
            chapter_dict_list = []
            for item in chapters:
                chapter_dict_list.append(item.toDict())
            return SuccessResponse(result = chapter_dict_list).toJson()
        else:
            chapters = query_chapters_by_bookid(book_info_request.get('book_id'))
            chapter_dict_list = []
            for item in chapters:
                chapter_dict_list.append(item.toDict())
            return ErrorResponse(message='书籍已存在或结果为空', result=chapter_dict_list).toJson(), 412
    else:
        return ErrorResponse(message='{}'.format(fm.toDict().get('error_message'))).toJson(), 412


@chapter.route('/api/chapter/getChapterInfo', methods=['POST'])
def get_chapter_info():
    document = beautiful_request('http://www.xbiquge.la/10/10489/9701158.html')
    beautifulBook = BeautifulChapterInfo(document)
    beautifulBook.getChapterTitle('div', 'bookname', None)
    beautifulChapter_title = beautifulBook.chapter_title
    return SuccessResponse(
        result={
            'chapterTitle': beautifulChapter_title,
        }
    ).toJson()
