from exts import db
from App.common.baseDict import BaseDict


class Chapter(BaseDict,db.Model):
    superKeys = (
        'id',
        'chapter_title',
        'chapter_url',
        'book_id'
    )
    id = db.Column(db.Integer, primary_key=True)
    chapter_title = db.Column(db.String(255))
    chapter_url = db.Column(db.String(255))
    book_id = db.Column(db.Integer)

    def __init__(self):
        BaseDict.__init__(self, superKeys=self.superKeys)