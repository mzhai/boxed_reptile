from flask import Blueprint, request, json

from App.apis.common.menu.model import Menu
from App.common.baseRequest import FormValidate
from App.common.baseResponse import SuccessResponse, ErrorResponse
from App.common.baseSession import BaseSession
from exts import db

menu = Blueprint('menu', __name__)


def query_menu_list():
    menuList = Menu.query.all()
    content = []
    for x in menuList:
        content.append(x.toDict())
    responce = {
        'count': len(menuList),
        'content': content
    }
    return responce


def query_menu_by_name(menu_label):
    menu_by_label = Menu.query.filter_by(label='{}'.format(menu_label)).first()
    return menu_by_label


def query_menu_by_id(id):
    menu_by_id = Menu.query.filter_by(id=id).first()
    return menu_by_id


@menu.route('/api/common/menu/list/')
@BaseSession.check
def menu_list():
    return SuccessResponse(result=query_menu_list()).toJson()


@menu.route('/api/common/menu/add/', methods=['POST'])
def menu_add():
    if request.method == 'POST':
        menu_request = {
            'label': request.form.get('label'),
            'icon': request.form.get('icon'),
            'value': request.form.get('value'),
        }
        query_menu = query_menu_by_name(menu_request.get('label'))
        fm = FormValidate()
        fm.required(menu_request)
        fm.valid()
        if not fm.toDict().get('error'):
            if query_menu is not None:
                return ErrorResponse(message='菜单名称重复').toJson(), 412
            else:
                menu_in = Menu()
                menu_in.label = menu_request.get('label')
                menu_in.icon = menu_request.get('icon')
                menu_in.value = menu_request.get('value')
                db.session.add(menu_in)
                db.session.commit()
                return SuccessResponse().toJson()
        else:
            return ErrorResponse(message='{}'.format(fm.toDict().get('error_message'))).toJson(), 412
    else:
        return ErrorResponse(message='GET not allowed').toJson()


@menu.route('/api/common/menu/update/')
def menu_update():
    return SuccessResponse(result=[])


@menu.route('/api/common/menu/delete/', methods=['POST'])
def menu_delete():
    menu_request = {
        'id': json.loads(request.data)['id']
    }
    query_menu = query_menu_by_id(menu_request.get('id'))
    fm = FormValidate()
    fm.required(menu_request)
    fm.valid()
    if not fm.toDict().get('error'):
        if query_menu is not None:
            db.session.delete(query_menu)
            db.session.commit()
            return SuccessResponse().toJson()
        else:
            return ErrorResponse(message='此菜单不存在').toJson(), 412
    else:
        return ErrorResponse(message='{}'.format(fm.toDict().get('error_message'))).toJson(), 412
