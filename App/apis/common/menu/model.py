from App.common.baseDict import BaseDict
from exts import db


class Menu(BaseDict, db.Model):
    superKeys = 'id', 'label', 'value', 'icon'
    id = db.Column(db.Integer, primary_key=True)
    label = db.Column(db.String(80))
    icon = db.Column(db.String(80))
    value = db.Column(db.String(80))

    def __init__(self):
        BaseDict.__init__(self, superKeys=self.superKeys)
