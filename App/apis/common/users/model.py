from flask_login import UserMixin

from App.common.baseDict import BaseDict
from exts import db


class User(db.Model, UserMixin, BaseDict):
    superKeys = 'user_id', 'accountNumber', 'name'
    user_id = db.Column('id', db.Integer, primary_key=True)
    accountNumber = db.Column(db.String(200), unique=True)
    password = db.Column(db.String(50), unique=True)
    name = db.Column(db.String(20), unique=True)

    def __init__(self, user_id=None, account_number=None, password=None, name="anonymous"):
        BaseDict.__init__(self, superKeys=self.superKeys)
        self.user_id = user_id
        self.accountNumber = account_number
        self.password = password
        self.name = name

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.user_id)

    def __repr__(self):
        return '<User %r>' % (self.accountNumber)
