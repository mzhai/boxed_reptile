from flask import Blueprint, request
from flask_login import login_user, login_required, logout_user

from App.apis.common.users.model import User
from App.common.baseRequest import FormValidate
from App.common.baseResponse import ErrorResponse, SuccessResponse
from App.common.baseSession import BaseSession
from exts import login_manager

user = Blueprint('user', __name__)  # 防止命名冲突


def query_user_by_accountpwd(accountNumber, password):
    return User.query.filter(accountNumber == accountNumber, password == password).first()


@login_manager.user_loader
def load_user(user_id):
    return User.query.filter_by(user_id=int(user_id)).first()


@user.route('/api/common/users/signin/', methods=['POST'])
def users_signin():
    signin_request = {
        'accountNumber': request.form.get('accountNumber'),
        'password': request.form.get('password'),
    }
    user_by_accountpwd = query_user_by_accountpwd(signin_request.get('accountNumber'), signin_request.get('password'))
    fm = FormValidate()
    fm.required(signin_request)
    fm.valid()
    if not fm.toDict().get('error'):
        if user_by_accountpwd is not None:
            login_user(user_by_accountpwd)
            return SuccessResponse(result=user_by_accountpwd.toDict()).toJson()
        else:
            return ErrorResponse(message='用户名或密码不正确').toJson(), 412
    else:
        return ErrorResponse(message='{}'.format(fm.toDict().get('error_message'))).toJson(), 412


@user.route('/api/common/users/signon/', methods=['POST'])
def users_signon():
    return 'users_signon'


@user.route('/api/common/users/signout/', methods=['POST'])
@login_required
def users_signout():
    logout_user()
    return 'users_signout'


@user.route('/api/common/users/checkState', methods=['GET'])
@BaseSession.check
def checkout_state():
    return SuccessResponse().toJson()
