import smtplib

from email.mime.text import MIMEText
from email.header import Header


# 实例
# smtplib.SMTP()：实例化SMTP()

# connect(host,port):

# host:指定连接的邮箱服务器。常用邮箱的smtp服务器地址如下：
#
# 新浪邮箱：smtp.sina.com,新浪VIP：smtp.vip.sina.com,搜狐邮箱：smtp.sohu.com，126邮箱：smtp.126.com,139邮箱：smtp.139.com,163网易邮箱：smtp.163.com。
#
# port：指定连接服务器的端口号，默认为25.
#
# login(user,password):
#
# user:登录邮箱的用户名。
#
# password：登录邮箱的密码，像笔者用的是网易邮箱，网易邮箱一般是网页版，需要用到客户端密码，需要在网页版的网易邮箱中设置授权码，该授权码即为客户端密码。
#
# sendmail(from_addr,to_addrs,msg,...):
#
# from_addr:邮件发送者地址
#
# to_addrs:邮件接收者地址。字符串列表['接收地址1','接收地址2','接收地址3',...]或'接收地址'
#
# msg：发送消息：邮件内容。一般是msg.as_string():as_string()是将msg(MIMEText对象或者MIMEMultipart对象)变为str。
#
# quit():用于结束SMTP会话。

username = '18170834715@163.com'
password = 'NZMMPGNVHIJSPHDH'
sender = '18170834715@163.com'
receivers = ['1014504021@qq.com']


def send_text_email(text):
    message = MIMEText(text,'plain', 'utf-8')
    message['From'] = Header("test", 'utf-8') #括号里的对应发件人邮箱昵称（随便起）、发件人邮箱账号
    message['To'] = Header("测试", 'utf-8') #括号里的对应收件人邮箱昵称、收件人邮箱账号
    subject = 'Python SMTP 邮件测试'
    message['Subject'] = Header(subject, 'utf-8')
    smtp = smtplib.SMTP()
    try:

        smtp.connect('smtp.163.com',25)
        smtp.login(username, password)
        smtp.sendmail(sender, receivers, message.as_string())
        print("邮件发送成功")
        smtp.quit()
        return {'success':True}
    except:
        print("Error: 无法发送邮件")
        return {'success':False}
