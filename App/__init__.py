from flask import Flask

from App.apis import init_apis
from config import init_config
from exts import init_db, init_login


def create_app(env):
    app = Flask(__name__, instance_relative_config=True)
    init_config(app, env)
    init_db(app)
    init_login(app)
    init_apis(app)
    return app
