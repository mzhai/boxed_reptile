from App.common.baseDict import BaseDict


class FormValidate(BaseDict):
    superKeys = ('required_errors', 'error', 'error_message')
    required_errors = []
    error = False
    error_message = ''

    def __init__(self):
        BaseDict.__init__(self, superKeys=self.superKeys)

    def required(self, fieldsDict):
        self.required_errors = []
        for item in list(fieldsDict.keys()):
            if (fieldsDict.get(item) is None) or (fieldsDict.get(item) == ''):
                self.required_errors.append(item)

    def required_spec(self, fieldsDict, keys):
        self.required_errors = []
        for item in keys:
            if (fieldsDict.get(item) is None) or (fieldsDict.get(item) == ''):
                self.required_errors.append(item)

    def valid(self):
        self.error_message = ''
        if len(self.required_errors) != 0:
            self.error = True
            self.error_message = '{0} required'.format(','.join(self.required_errors))
