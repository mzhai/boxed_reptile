from flask import jsonify

from App.common.baseDict import BaseDict


class ResponseState(BaseDict):
    superKeys = 'success', 'message', 'result'
    success = True
    message = None
    result = None

    def __init__(self, *, message=None, result=None):
        BaseDict.__init__(self, superKeys=self.superKeys)
        self.message = message
        self.result = result

    def toJson(self):
        return jsonify(self.toDict())


class ErrorResponse(ResponseState):
    success = False


class SuccessResponse(ResponseState):
    success = True
