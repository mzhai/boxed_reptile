from functools import wraps

from flask import session

from App.common.baseResponse import ErrorResponse


class BaseSession:
    @staticmethod
    def check(func):
        @wraps(func)  # 保存原来函数的所有属性,包括文件名
        def inner(*args, **kwargs):
            # 校验session
            session.permanent = True
            print(session)
            if session.get("_user_id") is not None:
                ret = func(*args, **kwargs)
                return ret
            else:
                return ErrorResponse(message='需要登录').toJson(), 401

        return inner

    @staticmethod
    def get(key):
        session_info = session.get(key)
        return session_info

    @staticmethod
    def clear():
        session.clear()

    @staticmethod
    def set(key, value):
        session[key] = value
