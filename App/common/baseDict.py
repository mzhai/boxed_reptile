class BaseDict:
    superKeys = None

    def __init__(self, *, superKeys):
        self.superKeys = superKeys

    def keys(self):
        return self.superKeys

    def __getitem__(self, item):
        return getattr(self, item)

    def toDict(self):
        return dict(self)
