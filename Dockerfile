FROM alpine:latest
COPY . /boxed
WORKDIR /boxed
ENV FLASK_ENV=testing
RUN apk update 
RUN apk add bash
RUN apk add supervisor
RUN apk add python3
RUN python3 get-pip.py
RUN pip install -r requirements.txt
RUN pip install gunicorn -i https://pypi.tuna.tsinghua.edu.cn/simple
RUN mkdir /var/log/supervisor
COPY supervisor_boxed.conf /etc/supervisor/conf.d/supervisor_boxed.conf
COPY supervisord.conf /etc/supervisor/supervisord.conf
CMD bash supervisord_start.sh
EXPOSE 8000