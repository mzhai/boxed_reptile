from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

login_manager = LoginManager()


def init_db(app):
    db.init_app(app)


def init_login(app):
    login_manager.login_view = '/api/users'
    login_manager.login_message_category = 'info'
    login_manager.login_message = 'Access denied.'
    login_manager.init_app(app)
    return login_manager
