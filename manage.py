import os
from flask_script import Manager
from App import create_app

env = os.environ.get('FLASK_ENV','development')
app = create_app(env)
app.secret_key = '!@#$%^&*()11'
manager = Manager(app=app)

if __name__ == '__main__':
    manager.run()