# -*- coding:utf-8 -*-
import sys
from os.path import abspath
from os.path import dirname
import manage

from multiprocessing.pool import ThreadPool

sys.path.insert(0, abspath(dirname(__file__)))


pool = ThreadPool(processes=1)



# 必须有一个叫做application的变量
# gunicorn 就要这个变量
# 这个变量的值必须是Flask的实力
application = manage.app

# 这是把代码部署到 apache gunicorn nginx 后面的套路
