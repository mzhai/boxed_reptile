'''
@Author: your name
@Date: 2020-05-26 11:35:07
@LastEditTime: 2020-05-26 14:17:48
@LastEditors: Please set LastEditors
@Description: In User Settings Edit
@FilePath: \boxed_reptile\config.py
'''
# Sqlite

# sqlite:///database.db
#
# #MySql
#
# mysql+pymysql://user:password@ip:port/db_name
#
# #Postgres
#
# postgresql+psycopg2://user:password@ip:port/db_name
#
# #MSSQL
#
# mssql+pyodbc://user:password@dsn_name
#
# #Orcale
#
# orcale+cx_orcale://user:password@ip:port/db_name

# app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(days=7)

from datetime import timedelta


def getDbUri(dbinfo):
    DB_ENGINE = dbinfo.get('DB_ENGINE') or "sqlite"
    DB_DRIVER = dbinfo.get('DB_DRIVER') or "sqlite"
    DB_USER = dbinfo.get('DB_USER') or ""
    DB_PASSWORD = dbinfo.get('DB_PASSWORD') or ""
    DB_HOST = dbinfo.get('DB_HOST') or "localhost"
    DB_PORT = dbinfo.get('DB_PORT') or ""
    DB_NAME = dbinfo.get('DB_NAME') or ""

    PERMANENT_SESSION_LIFETIME = timedelta(days=7)

    SQLALCHEMY_DATABASE_URI = dbinfo.get('DB_URI') or '{0}+{1}://{2}:{3}@{4}:{5}/{6}'.format(DB_ENGINE, DB_DRIVER,
                                                                                             DB_USER, DB_PASSWORD,
                                                                                             DB_HOST, DB_PORT, DB_NAME)
    return SQLALCHEMY_DATABASE_URI


class Config():
    DEBUG = False
    TESTING = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DB_INFO = {
        "DB_ENGINE": "",
        "DB_DRIVER": "",
        "DB_USER": "",
        "DB_PASSWORD": "",
        "DB_HOST": "",
        "DB_PORT": "",
        "DB_NAME": ""
    }
    JSON_AS_ASCII = False
    SQLALCHEMY_DATABASE_URI = getDbUri(DB_INFO)


class DevelopmentConfig(Config):
    DEBUG = True
    DB_INFO = {
        "DB_URI": "sqlite:///../develop.db",
    }
    SQLALCHEMY_DATABASE_URI = getDbUri(DB_INFO)
    PERMANENT_SESSION_LIFETIME = timedelta(days=7)


class TestingConfig(Config):
    TESTING = True
    DB_INFO = {
        "DB_URI": "sqlite:///../testing.db",
    }
    SQLALCHEMY_DATABASE_URI = getDbUri(DB_INFO)
    PERMANENT_SESSION_LIFETIME = timedelta(days=7)


class ProductConfig(Config):
    DB_INFO = {
        "DB_ENGINE": "",
        "DB_DRIVER": "",
        "DB_USER": "",
        "DB_PASSWORD": "",
        "DB_HOST": "",
        "DB_PORT": "",
        "DB_NAME": ""
    }
    SQLALCHEMY_DATABASE_URI = getDbUri(DB_INFO)
    PERMANENT_SESSION_LIFETIME = timedelta(days=7)


envs = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'product': ProductConfig,
    'default': DevelopmentConfig
}


def init_config(app, env):
    if env is not None:
        print('============== env ==============')
        print('>>>{}<<<'.format(envs.get(env).SQLALCHEMY_DATABASE_URI))
        print('============== env ==============')
        app.config.from_object(envs.get(env))
    else:
        print('error:none env')
