# Boxed_Reptile

### 介绍
```text
爬小说，文章，自定义爬取文字，一个项目
网络上这样的工具已经有很多
这个项目的意义仅仅是为了学习从开发到部署到生产监控的整个流程
```

### 开发
#### 使用python3 pip3
```
为什么选择python3?
简单，库多
```
#### 安装依赖
##### 比较重要的包
```bash
flask -- 框架
flask_login -- 登录验证session
flask_sqlalchemy -- 数据库管理
flask_script -- flask扩展管理，app管理--可以通过命令行的形式来操作Flask.例如通过命令跑一个开发的服务器、设置数据库等
bs4 -- 大家都懂
```

##### 依赖文件(类似package.json或pubspec.yaml)
生成requirements.txt文件
```bash
pip freeze > requirements.txt
```

安装requirements.txt依赖
```bash
pip install -r requirements.txt
```
#### 虚拟环境
##### winodws
```bash
py3/Scripts/activate.bat
```
##### linux (unix)
```bash
source venv/bin/activate  
```
#### 设置环境变量
```bash
FLASK_ENV=development | testing | product
```
#### 启动开发服务器

```bash
python3 manage.py runserver
```

### 直接部署 (Linux)

#### 0.intro  Gunicorn
https://www.cnblogs.com/hellohorld/p/10033720.html
https://www.cnblogs.com/cuzz/p/8146436.html
<br />
Gunicorn如何部署Flask网站，直接看Flask或Gunicorn官方文件即可，通常只要执行类似下面的一行命令：
``` bash
/usr/local/bin/gunicorn -w 2 -b :4000 manage:app
```
#### 1.Touch wsgi.py
```python
# -*- coding:utf-8 -*-
import sys
from os.path import abspath
from os.path import dirname

sys.path.insert(0, abspath(dirname(__file__)))

import manage

# 必须有一个叫做application的变量
# gunicorn 就要这个变量
# 这个变量的值必须是Flask的实力
application = manage.manager

# 这是把代码部署到 apache gunicorn nginx 后面的套路
```
#### 2.apt-get install gunicorn  python3安装gunicorn3
```bash
apt-get install gunicorn3
```

```bash
gunicorn3 wsgi:application  --bind 0.0.0.0:8000 --pid /tmp/boxed.pid
```

#### 3. 在根目录下安装supervisor
https://www.jianshu.com/p/0b9054b33db3
<br />
```bash
apt-get install supervisor  
```

#### 4.给我们自己开发的应用程序编写一个配置文件，让supervisor来管理它。
https://blog.csdn.net/yfanjy/article/details/105975723
<br />
每个进程的配置文件都可以单独分拆，放在/etc/supervisor/conf.d/目录下，以.conf作为扩展名，

例如，boxed.conf定义了一个gunicorn的进程

```bash
nano /etc/supervisor/conf.d/boxed.conf
```

boxed.conf内容：
```bash
[program:boxed]
command=gunicorn3 wsgi:application  --bind 0.0.0.0:8000 --pid /tmp/boxed.pid
directory=/root/git/boxed_reptile/
autostart=true
autorestart=true
```

#### 5.启动所有进程
```bash
supervisord -c supervisord.conf 
```

如果有如下错误:
```bash
Error: Another program is already listening on a port that one of our HTTP servers is configured to
```
解决方法
```bash
find / -name supervisor.sock
unlink /｛name | run｝/supervisor.sock # ｛name | run｝值得是上一句命令的结果位置
```

#### 6.结束进程
```bash
supervisorctl stop all
```

### Docker部署

#### Dockerfile

```dockerfile
# 基础容器alpine
FROM alpine:latest  
# 将本项目根目录 拷贝到docker镜像中的 /boxed 文件夹中     
COPY . /boxed   
# 将/boxed文件夹设为工作目录         
WORKDIR /boxed   
# 设置环境变量          
ENV FLASK_ENV=testing    
# 运行update 并安装依赖包
RUN apk update 
RUN apk add bash
RUN apk add supervisor
RUN apk add python3
RUN python3 get-pip.py
# 安装 项目python依赖
RUN pip install -r requirements.txt
# 安装gunicorn服务
RUN pip install gunicorn -i https://pypi.tuna.tsinghua.edu.cn/simple
# 配置supervisor服务
RUN mkdir /var/log/supervisor
COPY supervisor_boxed.conf /etc/supervisor/conf.d/supervisor_boxed.conf
COPY supervisord.conf /etc/supervisor/supervisord.conf
# 通过项目根目录下的supervisord_start.sh来启动supervisor服务
CMD bash supervisord_start.sh
# 暴露8000端口
EXPOSE 8000
```

#### 通过Dockerfile打包image

```bash
docker image build -t boxed-t-0.0.92 .
```

#### 生成Container
```bash
docker container run \
 -d  \
--rm \
--name boxed-t-0.0.92  \
-p 8000:8000 \
-i -t baby00700/boxedreptile-t:0.0.92 \
bash supervisord_start.sh
```

### TODO

#### 绕过IP封锁
目前想到的有Tor和代理代理池的方式，暂时还没有什么好的办法,先占个坑，有办法之后再补回来
```
1.Tor ...
2.Proxy ...
```

#### 开启定时任务
#### 加入日志服务
#### 前端界面
这是一个FEer的执念